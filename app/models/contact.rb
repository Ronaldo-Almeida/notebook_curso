#Se contato pertencer a mais de um entao a conversão é :addresses
class Contact < ApplicationRecord
  belongs_to :kind
  has_one  :address, :dependent => :destroy
  has_many :phones, :dependent => :destroy

  validates :name, presence: true
  validates :email, presence: true

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :phones, reject_if: :all_blank, allow_destroy: true
end
