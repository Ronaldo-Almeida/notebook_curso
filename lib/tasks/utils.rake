#para gerar esta pagina foi utilizado rails g task utils seed
namespace :utils do
  desc "Popular banco de dados."
  task seed: :environment do

        puts "Gerando  CONTATOS"
        50.times do |i|
        Contact.create!(
          name:Faker::Name.name,
          email:Faker::Internet.email,
          kind:Kind.all.sample,
          rmk:Faker::Lorem.paragraph([1,2,3,4,5].sample)
        )
        end
        puts "CONTATOS inserido com sucesso!"

        puts "Gerando  PHONES"
        Contact.all.each do |contact|
        Phone.create!(
          phone:Faker::PhoneNumber.subscriber_number(8),
          contact:contact
        )
        end
        puts "Telefones inseridos com sucesso!"

        puts "Gerando  ENDEREÇOS"
        Contact.all.each do |contact|
        Address.create!(
          street: Faker::Address.street_address,
          city: Faker::Address.city,
          state: Faker::Address.state,
          contact: contact
        )
        end
        puts "ENDEREÇOS inseridos com sucesso!"
  end

end
